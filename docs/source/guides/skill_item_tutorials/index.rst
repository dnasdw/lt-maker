Skill and Item Tutorials
========================

These are guides on various useful skills and items.

.. toctree::
    :maxdepth: 2
    :caption: Basic

    Creating-Items
    Direct-Passives
    Conditional-Passives-I
    Auras
    Conditional-Passives-II

.. toctree::
    :maxdepth: 2
    :caption: Advanced

    Creating-Capture
    Recruit-Skill
    Summoning
    Multi-Items